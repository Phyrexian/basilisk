CXX = g++
CXX_FLAGS = -std=c++17 -g -O2 -Wall
SRC = src
INCLUDE = include
LIB = lib
LIBRARIES = -lpthread -larmadillo

.PHONY: all clean run

all: server.out alphasnake.out

run:
	./server.out

clean:
	rm server.out
	rm alphasnake.out

server.out: $(SRC)/server.cpp $(SRC)/*.hpp
	$(CXX) $^ $(CXX_FLAGS) -I$(SRC) -I$(INCLUDE) -L$(LIB) $(LIBRARIES) -o $@

alphasnake.out: $(SRC)/alphasnake.cpp $(SRC)/*.hpp
	$(CXX) $^ $(CXX_FLAGS) -I$(SRC) -I$(INCLUDE) -L$(LIB) $(LIBRARIES) -o $@
