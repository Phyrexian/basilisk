#ifndef MOVE_HPP
#define MOVE_HPP

#include <iostream>

#include "battlesnake.hpp"


namespace basilisk {
    namespace bs = battlesnake;

    std::vector<std::vector<int>> expected_moves(bs::Board &board) {
        std::vector<std::vector<int>> moves(board.snakes.size());
        for (size_t i = 0; i < board.snakes.size(); i++) {
            std::vector<bool> ok(4, true);
            for (size_t j = 0; j < 4; j++) {
                bs::Point dest = board.snakes[i].body.front() + bs::DIRS[j];
                if (board.out_of_bounds(dest)) {
                    ok[j] = false;
                    continue;
                }
                for (size_t k = 0; k < board.snakes.size(); k++)
                    if (std::count(board.snakes[k].body.begin(), std::prev(board.snakes[k].body.end()), dest)) {
                        ok[j] = false;
                        break;
                    }
            }
            for (size_t j = 0; j < 4; j++)
                if (ok[j])
                    moves[i].push_back(j);
            if (!moves[i].size())
                for (size_t j = 0; j < 4; j++)
                    moves[i].push_back(j);
        }
        return moves;
    }

    std::pair<double, int> random_game(bs::Board &board, const std::string &id, const int &max_depth, int curr_depth = 0) {
        if (!board.snakes.size())
            return { 0.0, -1 };
        int im = -1;
        for (size_t i = 0; i < board.snakes.size(); i++)
            if (board.snakes[i].id == id)
                im = i;
        if (im == -1)
            return { 0.0, -1 };
        if (board.snakes.size() == 1)
            return { 1.0, -1 };
        if (curr_depth == max_depth)
            return { 1.0 / board.snakes.size(), -1 };

        std::vector<std::vector<int>> expected = expected_moves(board);
        std::vector<int> chosen(board.snakes.size());
        for (size_t i = 0; i < board.snakes.size(); i++) {
            std::uniform_int_distribution<> distr(0, expected[i].size() - 1);
            long long index = distr(eng);
            chosen[i] = expected[i][index];
        }
        board.advance(chosen);
        return { random_game(board, id, max_depth, curr_depth + 1).first, chosen[im] };
    }

    bs::Response move(bs::Request request) {
        std::vector<double> rewards(4);
        std::vector<int> games(4);
        long long start_time = curr_time();
        while (curr_time() - start_time < 300000000) {
            double reward;
            int action;
            bs::Board board_copy = request.board;
            std::tie(reward, action) = random_game(board_copy, request.you.id, request.board.width + request.board.height - 2);
            rewards[action] += reward;
            games[action]++;
        }

        std::cout << games << std::endl;

        int best = -1;
        for (int i = 0; i < 4; i++)
            if (games[i] && (best == -1 || rewards[i] * games[best] > rewards[best] * games[i]))
                best = i;
        return bs::Response{bs::NAMES[best], ""};
    }
}

#endif