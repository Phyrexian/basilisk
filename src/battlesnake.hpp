#ifndef BATTLESNAKE_HPP
#define BATTLESNAKE_HPP

#include <list>
#include <set>
#include <string>
#include <vector>

#include "nlohmann/json.hpp"
#include "util.hpp"

namespace battlesnake {
    struct Ruleset {
        std::string name;
        std::string version;
        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Ruleset, name, version)
    };

    struct Game {
        std::string id;
        Ruleset ruleset;
        int timeout;
        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Game, id, ruleset, timeout)
    };

    struct Point {
        int x;
        int y;
        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Point, x,  y)

        Point &operator+=(const Point &rhs) {
            x += rhs.x;
            y += rhs.y;
            return *this;
        }

        Point &operator-=(const Point &rhs) {
            x -= rhs.x;
            y -= rhs.y;
            return *this;
        }

        friend Point operator+(Point lhs, const Point &rhs) {
            lhs += rhs;
            return lhs;
        }

        friend Point operator-(Point lhs, const Point &rhs) {
            lhs -= rhs;
            return lhs;
        }
    };

    std::ostream &operator<<(std::ostream &os, const Point &point) {
        return os << '[' << point.x << ", " << point.y << ']';
    }

    inline bool operator<(const Point &lhs, const Point &rhs) { return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y); }
    inline bool operator>(const Point &lhs, const Point &rhs) { return rhs < lhs; }
    inline bool operator<=(const Point &lhs, const Point &rhs) { return !(lhs > rhs); }
    inline bool operator>=(const Point &lhs, const Point &rhs) { return !(lhs < rhs); }
    inline bool operator==(const Point &lhs, const Point &rhs) { return lhs.x == rhs.x && lhs.y == rhs.y; }
    inline bool operator!=(const Point &lhs, const Point &rhs) { return !(lhs == rhs); }

    struct Snake {
        std::string id;
        std::string name;
        int health;
        std::list<Point> body;
        std::string latency;
        Point head;
        int length;
        std::string shout;
        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Snake, id, name, health, body, latency, head, length, shout)
    };

    const int SNAKE_MAX_HEALTH = 100;
    const int SNAKE_START_SIZE = 3;
    const int FOOD_SPAWN_CHANCE = 15;
    const int MINIMUM_FOOD = 1;
    const std::vector<Point> DIRS{ Point{0, 1}, Point{0, -1}, Point{1, 0}, Point{-1, 0} };
    const std::vector<std::string> NAMES{ "up", "down", "right", "left" };

    struct Board {
        int height;
        int width;
        std::set<Point> food;
        std::set<Point> hazards;
        std::vector<Snake> snakes;
        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Board, height, width, food, hazards, snakes)

        bool out_of_bounds(const Point &point) const {
            return point.x < 0 || point.x >= width || point.y < 0 || point.y >= height;
        }

        std::vector<Point> unoccupied() {
            std::vector<std::vector<bool>> occupied(width, std::vector<bool>(height));
            for (size_t i = 0; i < snakes.size(); i++)
                for (Point point : snakes[i].body)
                    if (!out_of_bounds(point))
                        occupied[point.x][point.y] = true;
            for (Point point : food)
                occupied[point.x][point.y] = true;
            std::vector<Point> points;
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    if (!occupied[x][y])
                        points.push_back(Point{x, y});
            return points;
        }

        void spawn_food(size_t n) {
            std::vector<Point> options = unoccupied();
            std::shuffle(options.begin(), options.end(), eng);
            for (size_t i = 0; i < std::min(n, options.size()); i++)
                food.insert(options[i]);
        }

        void maybe_spawn_food() {
            if (food.size() < MINIMUM_FOOD)
                spawn_food(MINIMUM_FOOD - food.size());
            else if (FOOD_SPAWN_CHANCE > 0 && randint(99) < FOOD_SPAWN_CHANCE)
                spawn_food(1);
        }

        void advance(const std::vector<int> &actions, bool dont_spawn_food = false) {
            assert(snakes.size() == actions.size());
            for (size_t i = 0; i < snakes.size(); i++) {
                snakes[i].body.push_front(snakes[i].body.front() + DIRS[actions[i]]);
                snakes[i].body.pop_back();
                snakes[i].health -= 1;
            }
            std::set<Point> eaten;
            for (size_t i = 0; i < snakes.size(); i++)
                if (food.count(snakes[i].body.front())) {
                    snakes[i].health = SNAKE_MAX_HEALTH;
                    snakes[i].body.push_back(snakes[i].body.back());
                    eaten.insert(snakes[i].body.front());
                }
            std::set<Point> new_food;
            std::set_difference(food.begin(), food.end(), eaten.begin(), eaten.end(), std::inserter(new_food, new_food.begin()));
            food = new_food;
            if (!dont_spawn_food)
                maybe_spawn_food();
            std::vector<bool> elim(snakes.size());
            for (size_t i = 0; i < snakes.size(); i++) {
                if (snakes[i].health <= 0) {
                    elim[i] = true;
                    continue;
                }
                if (out_of_bounds(snakes[i].body.front())) {
                    elim[i] = true;
                    continue;
                }
                for (size_t j = 0; j < snakes.size(); j++) {
                    if (std::count(std::next(snakes[j].body.begin()), snakes[j].body.end(), snakes[i].body.front())) {
                        elim[i] = true;
                        break;
                    }
                    if (i != j && snakes[i].body.front() == snakes[j].body.front() && snakes[i].body.size() <= snakes[j].body.size()) {
                        elim[i] = true;
                        break;
                    }
                }
            }
            std::vector<Snake> new_snakes;
            for (size_t i = 0; i < snakes.size(); i++)
                if (!elim[i])
                    new_snakes.push_back(snakes[i]);
            snakes = new_snakes;
            for (size_t i = 0; i < snakes.size(); i++) {
                snakes[i].head = snakes[i].body.front();
                snakes[i].length = snakes[i].body.size();
            }
        }

        int index(const std::string &id) const {
            for (size_t i = 0; i < snakes.size(); i++)
                if (snakes[i].id == id)
                    return i;
            return -1;
        }

        std::vector<std::string> ids() const {
            std::vector<std::string> v(snakes.size());
            for (size_t i = 0; i < snakes.size(); i++)
                v[i] = snakes[i].id;
            return v;
        }

        static Board random(int side, int players) {
            int near = 1;
            int far = side - 2;
            int center = (near + far) / 2;
            std::vector<Point> options{
                Point{near, near},
                Point{near, center},
                Point{near, far},
                Point{center, near},
                Point{center, far},
                Point{far, near},
                Point{far, center},
                Point{far, far}
            };
            std::shuffle(options.begin(), options.end(), eng);
            std::vector<Snake> snakes(players);
            for (int i = 0; i < players; i++)
                snakes[i] = Snake{
                    std::to_string(i),
                    std::to_string(i),
                    SNAKE_MAX_HEALTH,
                    std::list<Point>(SNAKE_START_SIZE, options[i]),
                    "0",
                    options[i],
                    SNAKE_START_SIZE,
                    ""
                };
            options = std::vector<Point>{
                Point{-1, -1},
                Point{-1, 1},
                Point{1, -1},
                Point{1, 1}
            };
            std::set<Point> food;
            static std::uniform_int_distribution<> distr(0, 3);
            for (int i = 0; i < players; i++)
                food.insert(snakes[i].head + options[distr(eng)]);
            food.insert(Point{center, center});
            return Board{side, side, food, std::set<Point>(), snakes};
        }
    };

    std::ostream &operator<<(std::ostream &os, const Board &board) {
        std::vector<std::string> grid(board.height, std::string(board.width, '.'));
        for (size_t i = 0; i < board.snakes.size(); i++) {
            for (Point point : board.snakes[i].body)
                grid[board.height - point.y - 1][point.x] = '#';
            Point head = *board.snakes[i].body.begin();
            grid[board.height - head.y - 1][head.x] = '0' + i;
        }
        for (Point point : board.food)
            grid[board.height - point.y - 1][point.x] = '*';
        for (int i = 0; i < board.height; i++)
            os << grid[i] << '\n';
        return os;
    }

    struct Request {
        Game game;
        int turn;
        Board board;
        Snake you;
        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Request, game, turn, board, you)
    };

    struct Response {
        std::string move;
        std::string shout;
        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Response, move, shout)
    };
}

#endif