#ifndef ALPHASNAKE_HPP
#define ALPHASNAKE_HPP

#include <armadillo>
#include <cmath>
#include <deque>
#include <fstream>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include "battlesnake.hpp"
#include "util.hpp"

const int FEATURES = 36;

void read_arma_vec(std::istream &is, arma::vec &v) {
    for (size_t i = 0; i < v.size(); i++)
        is >> v.at(i);
}

void write_arma_vec(std::ostream &os, const arma::vec &v) {
    for (size_t i = 0; i < v.size(); i++)
        os << v.at(i) << '\n';
}

struct AlphaSnake {
    std::vector<arma::vec> w;

    AlphaSnake(const std::string &wpath="") {
        w.assign(4, arma::vec(FEATURES, arma::fill::zeros));
        if (!wpath.empty())
            load_weights(wpath);
    }

    void load_weights(const std::string &path) {
        std::ifstream ifile(path);
        if (!ifile.is_open()) {
            std::cout << "Failed to open " << path << "\n";
            return;
        }
        for (size_t i = 0; i < 4; i++)
            read_arma_vec(ifile, w[i]);
    }

    void save_weights(const std::string &path) {
        std::ofstream ofile(path);
        if (!ofile.is_open()) {
            std::cout << "Failed to open " << path << "\n";
            return;
        }
        for (size_t i = 0; i < 4; i++)
            write_arma_vec(ofile, w[i]);
    }

    static arma::vec get_features(const battlesnake::Board &board, int player) {
        const double D = 2;
        std::vector<double> features;
        for (int x = -D; abs(x) <= D; x++)
            for (int y = abs(x) - D; abs(x) + abs(y) <= D; y++) {
                if (x || y) {
                    battlesnake::Point dest = board.snakes[player].head + battlesnake::Point{x, y};

                    double taken = 0.0;
                    if (board.out_of_bounds(dest))
                        taken = 1.0;
                    for (size_t i = 0; i < board.snakes.size(); i++)
                        if (std::count(board.snakes[i].body.begin(), std::prev(board.snakes[i].body.end()), dest))
                            taken = 1.0;
                    features.push_back(taken);

                    double head = 0.0;
                    for (size_t i = 0; i < board.snakes.size(); i++)
                        if (board.snakes[i].head == dest && board.snakes[i].length >= board.snakes[player].length)
                            head = 1.0;
                    features.push_back(head);

                    double food = 0.0;
                    if (board.food.count(dest))
                        food = 1.0 - board.snakes[player].health / 100.0;
                    features.push_back(food);
                }
            }
        return arma::conv_to<arma::vec>::from(features);
    }

    int greedy_action(const arma::vec &s) {
        double max = -std::numeric_limits<double>::max();
        int argmax = -1;
        for (int i = 0; i < 4; i++) {
            double d = arma::dot(s, w[i]);
            if (d > max) {
                max = d;
                argmax = i;
            }
        }
        return argmax;
    }

    void learn(double gamma, int episodes) {
        arma::mat EPSILON = 1e-9 * arma::mat(FEATURES, FEATURES, arma::fill::eye);
        std::vector<arma::mat> A_SUM(4, arma::mat(FEATURES, FEATURES, arma::fill::zeros));
        std::vector<arma::vec> B_SUM(4, arma::vec(FEATURES, arma::fill::zeros));
        std::vector<std::deque<arma::mat>> A(4);
        std::vector<std::deque<arma::mat>> B(4);
        for (size_t i = 0; i < 4; i++) {
            for (size_t j = 0; j < 1; j++) {
                A[i].push_back(arma::mat(FEATURES, FEATURES, arma::fill::zeros));
                B[i].push_back(arma::vec(FEATURES, arma::fill::zeros));
            }
        }
        for (int ep = 1; ep <= episodes; ep++) {
            double epsilon = 1.0 / ep;
            battlesnake::Board board = battlesnake::Board::random(11, 2);
            std::vector<std::vector<arma::vec>> states(2);
            std::vector<std::vector<int>> actions(2);
            std::vector<std::vector<double>> rewards(2);
            for (int i = 0; i < 2; i++)
                states[i].push_back(get_features(board, i));
            while (board.snakes.size() > 1) {
                for (int i = 0; i < 2; i++) {
                    if (randreal(1) < epsilon)
                        actions[i].push_back(randint(3));
                    else
                        actions[i].push_back(greedy_action(states[i].back()));
                }
                std::vector<int> a(2);
                for (int i = 0; i < 2; i++)
                    a[i] = actions[i].back();
                board.advance(a);
                for (int i = 0; i < 2; i++) {
                    states[i].push_back(get_features(board, i));
                    if (board.index(std::to_string(i)) == -1)
                        rewards[i].push_back(-1);
                    else if (board.snakes[i].health == battlesnake::SNAKE_MAX_HEALTH)
                        rewards[i].push_back(0.1);
                    else
                        rewards[i].push_back(0.01);
                }
            }
            if (ep % 1000 == 0)
                std::cout << actions[0].size() << '\n';
            std::vector<arma::mat> A_PRIME(4, arma::mat(FEATURES, FEATURES, arma::fill::zeros));
            std::vector<arma::vec> B_PRIME(4, arma::vec(FEATURES, arma::fill::zeros));
            for (int i = 0; i < 2; i++) {
                for (size_t t = 0; t < rewards.size(); t++) {
                    A_PRIME[actions[i][t]] += states[i][t] * (states[i][t] - (t == rewards.size() - 1 ? 0 : gamma) * states[i][t + 1]).t();
                    B_PRIME[actions[i][t]] += states[i][t] * rewards[i][t];
                }
            }
            for (int i = 0; i < 4; i++) {
                //A_SUM[i] -= A[i].front();
                //B_SUM[i] -= B[i].front();
                A[i].pop_front();
                B[i].pop_front();
                A_SUM[i] += A_PRIME[i];
                B_SUM[i] += B_PRIME[i];
                A[i].push_back(A_PRIME[i]);
                B[i].push_back(B_PRIME[i]);
                w[i] = arma::inv(A_SUM[i] + EPSILON) * B_SUM[i];
            }
        }
    }

    battlesnake::Response move(battlesnake::Request request) {
        arma::vec state = get_features(request.board, request.board.index(request.you.id));
        return battlesnake::Response{battlesnake::NAMES[greedy_action(state)], ""};
    }
};

#endif