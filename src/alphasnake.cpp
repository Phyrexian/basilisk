#include "alphasnake.hpp"

int main() {
    AlphaSnake alpha("weights.txt");
    alpha.learn(0.9, 1000000);
    alpha.save_weights("weights.txt");
}