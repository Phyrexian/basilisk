// HTTP credits: https://github.com/yhirose/cpp-httplib
// JSON credits: https://github.com/nlohmann/json
#include <iostream>

#include "nlohmann/json.hpp"
#include "alphasnake.hpp"
#include "basilisk.hpp"
#include "battlesnake.hpp"
#include "httplib.h"
#include "mcts.hpp"

int main() {
    httplib::Server svr;
    //AlphaSnake alphasnake("weights.txt");
    svr.Get("/", [](const auto &, auto &res) {
        std::string head = "default";
        std::string tail = "default";
        std::string author = "Phyrexian";
        std::string color = "#7d9883";
        res.set_content("{\"apiversion\":\"1\", \"head\":\"" + head + "\", \"tail\":\"" + tail + "\", \"color\":\"" + color + "\", " +
                            "\"author\":\"" + author + "\"}",
                        "text/json");
    });
    svr.Post("/end", [](const auto &, auto &res) { res.set_content("ok", "text/plain"); });
    svr.Post("/start", [](const auto &, auto &res) { res.set_content("ok", "text/plain"); });
    svr.Post("/move", [](auto &req, auto &res) {
        nlohmann::json json = nlohmann::json::parse(req.body);
	    json = mcts::move(json.get<battlesnake::Request>());
        res.set_content(json.dump(), "text/plain");
    });
    svr.listen("0.0.0.0", 8081);
}
