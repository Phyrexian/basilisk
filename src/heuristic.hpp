#ifndef HEURISTIC_HPP
#define HEURISTIC_HPP

#include <queue>
#include <vector>

#include "battlesnake.hpp"

namespace heuristic {
    namespace bs = battlesnake;

    std::vector<double> control(const bs::Board &board) {
        std::vector<std::vector<int>> ticks(board.width, std::vector<int>(board.height));
        for (size_t i = 0; i < board.snakes.size(); i++) {
            int j = board.snakes[i].body.size();
            for (const auto &bp : board.snakes[i].body)
                ticks[bp.x][bp.y] = std::max(ticks[bp.x][bp.y], j--);
        }
        std::vector<std::vector<std::vector<int>>> dists(board.snakes.size());
        for (size_t i = 0; i < board.snakes.size(); i++) {
            dists[i].assign(board.width, std::vector<int>(board.height, std::numeric_limits<int>::max()));
            std::queue<bs::Point> Q;
            Q.push(board.snakes[i].head);
            while (!Q.empty()) {
                bs::Point p = Q.front();
                Q.pop();

                int D = dists[i][p.x][p.y];
                if (D == std::numeric_limits<int>::max())
                    D = 0;
                for (int j = 0; j < 4; j++) {
                    bs::Point q = p + bs::DIRS[j];
                    if (board.out_of_bounds(q))
                        continue;
                    if (dists[i][q.x][q.y] != std::numeric_limits<int>::max())
                        continue;
                    if (ticks[q.x][q.y] > D + 1)
                        continue;
                    dists[i][q.x][q.y] = D + 1;
                    Q.push(q);
                }
            }
        }
        std::vector<double> cnt(board.snakes.size());
        for (int x = 0; x < board.width; x++)
            for (int y = 0; y < board.height; y++) {
                std::pair<int, int> min{ std::numeric_limits<int>::max(), 0 };
                std::vector<int> min_snakes;
                for (size_t i = 0; i < board.snakes.size(); i++) {
                    std::pair<int, int> pair{ dists[i][x][y], -board.snakes[i].length };
                    if (pair < min) {
                        min = pair;
                        min_snakes = { (int)i };
                    } else if (pair == min)
                        min_snakes.push_back(i);
                }
                if (min.first == std::numeric_limits<int>::max())
                    continue;
                for (size_t i = 0; i < min_snakes.size(); i++)
                    cnt[min_snakes[i]] += 1.0 / min_snakes.size();
            }
        return cnt;
    }
}

#endif