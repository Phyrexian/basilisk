#ifndef MCTS_HPP
#define MCTS_HPP

#include <cmath>
#include <limits>
#include <map>
#include <memory>
#include <numeric>
#include <vector>

#include "battlesnake.hpp"
#include "heuristic.hpp"

namespace mcts {
    namespace bs = battlesnake;

    struct Node {
        int visits;
        std::vector<std::vector<double>> child_values;
        std::vector<std::vector<int>> child_visits;
        std::map<int, std::unique_ptr<Node>> children; 

        Node(int n) {
            visits = 0;
            child_values.assign(n, std::vector<double>(4, 0.0));
            child_visits.assign(n, std::vector<int>(4, 0));      
        }

        std::vector<double> simulate(battlesnake::Board &board, const std::vector<std::string> &ids) {
            visits += 1;
            std::vector<int> ucb_actions(ids.size(), 7), board_actions(board.snakes.size(), 7);
            for (size_t i = 0; i < board.snakes.size(); i++) {
                int pi = std::find(ids.begin(), ids.end(), board.snakes[i].id) - ids.begin();
                double ucb_max = -std::numeric_limits<double>::max();
                std::vector<int> ucb_max_actions;
                for (int j = 0; j < 4; j++) {
                    double ucb = child_values[pi][j] / std::max(child_visits[pi][j], 1) + sqrt(2) * sqrt(log(visits) / (child_visits[pi][j] + 1));
                    if (ucb > ucb_max) {
                        ucb_max = ucb;
                        ucb_max_actions = { j };
                    } else if (ucb == ucb_max)
                        ucb_max_actions.push_back(j);
                }
                ucb_actions[pi] = board_actions[i] = ucb_max_actions[randint(ucb_max_actions.size() - 1)];
            }

            board.advance(board_actions, true);
            int action_bits = 0;
            for (int i = ids.size() - 1; i >= 0; i--)
                action_bits = 8 * action_bits + ucb_actions[i];
            std::vector<double> results(ids.size(), 0.0);
            if (board.snakes.size() > 1) {
                if (children.count(action_bits))
                    results = children[action_bits]->simulate(board, ids);
                else {
                    std::vector<double> cnt = heuristic::control(board);
                    double sum = std::accumulate(cnt.begin(), cnt.end(), 0.0);
                    results.assign(ids.size(), -1.0);
                    for (size_t i = 0; i < board.snakes.size(); i++) {
                        int pi = std::find(ids.begin(), ids.end(), board.snakes[i].id) - ids.begin();
                        results[pi] = (cnt[i] / std::max(sum, 1.0) - 0.5) * 2;
                    }
                }
            } else if (board.snakes.size() == 1) {
                for (size_t i = 0; i < ids.size(); i++)
                    if (board.index(ids[i]) == -1)
                        results[i] = -1.0;
                    else
                        results[i] = 1.0;
            }

            if (!children.count(action_bits))
                children[action_bits] = std::make_unique<Node>(ids.size());

            for (size_t i = 0; i < ids.size(); i++) {
                child_values[i][ucb_actions[i]] += results[i];
                child_visits[i][ucb_actions[i]] += 1;
            }
            return results;
        }

        std::vector<int> choose_moves(const std::vector<std::string> &ids) {
            std::vector<int> actions(ids.size(), 7);
            for (size_t i = 0; i < ids.size(); i++) {
                int visits_max = std::numeric_limits<int>::min();
                std::vector<int> visits_max_actions;
                for (int j = 0; j < 4; j++)
                    if (child_visits[i][j] > visits_max) {
                        visits_max = child_visits[i][j];
                        visits_max_actions = { j };
                    } else if (child_visits[i][j] == visits_max)
                        visits_max_actions.push_back(j);
                actions[i] = visits_max_actions[randint(visits_max_actions.size() - 1)];
            }
            return actions;
        }
    };

    bs::Response move(const bs::Request &request) {
        bs::Board board = request.board;
        std::vector<std::string> ids = board.ids();
        int me = board.index(request.you.id);
        mcts::Node root(ids.size());
        long long start_time = curr_time();
        while (curr_time() - start_time < 280000000) {
            bs::Board board_copy = board;
            root.simulate(board_copy, ids);
        }
        std::string my_move = bs::NAMES[root.choose_moves(ids)[me]];
        return bs::Response{my_move, ""};
    }
}

#endif