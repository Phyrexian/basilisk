#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>
#include <limits>
#include <chrono>
#include <random>

template<typename T> std::ostream &operator<<(std::ostream &os, const std::vector<T> &c) {
    for (auto it = c.begin(); it != c.end(); it++) {
        if (it != c.begin())
            os << " ";
        os << *it;
    }
    return os;
}

long long curr_time() {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

long long _start_time = curr_time();

long long exec_time() {
    return curr_time() - _start_time;
}

std::mt19937_64 eng(_start_time);

int randint(int a, int b) {
    std::uniform_int_distribution<> distr(a, b);
    return distr(eng);
}

int randint(int a) {
    return randint(0, a);
}

double randreal(int a, int b) {
    std::uniform_real_distribution<> distr(a, b);
    return distr(eng);
}

double randreal(double a) {
    return randreal(0, a);
}

#endif